
import React from 'react';
import PropTypes from 'prop-types';
import './price.scss';

const Price = (props) =>{
    const price =  props.price % 1 === 0  && props.currency === '£' ? `${props.currency}${((props.price)/100).toFixed(2)}` : false;
    return  price ? <span  className='price'>{price}</span> : <span className='price-error'>Error</span>;
}

export default Price ;

Price.PropTypes = {
    price:PropTypes.number,
    currency:PropTypes.string,
}
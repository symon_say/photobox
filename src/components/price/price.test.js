import React from 'react';
import Price from './index'
import { shallow,mount } from 'enzyme';
require ('../../setupTests.js');



describe("Price component",()=>{
    const props ={
        price:1000,
        currency:"£"
    }
    const price = shallow(<Price {...props}/>);

    it("Price renders",()=>{
        expect(price).toMatchSnapshot();
    })
    it("Have a class of `Price`",()=>{
        expect(price.find(".price").exists()).toBe(true);
    })
    it("It renders the text correctly",()=>{
         expect(price.find(".price").text()).toEqual("£10.00");
     })
    it("It renders the error message if the price is a string",()=>{ 
        props.price = "acd"; 
        const price = shallow(<Price {...props}/>);
       expect(price.text()).toBe("Error");
    })
    it("It renders the error message if the currency is missing",()=>{ 
        props.price = 1000; 
        props.currency = "l"; 
        const price = shallow(<Price {...props}/>);
       expect(price.text()).toBe("Error");
    })
})
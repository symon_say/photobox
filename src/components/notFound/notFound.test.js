import React from 'react';
import NotFound from './index'
import { shallow } from 'enzyme';
require ('../../setupTests.js');

const notFound = shallow(<NotFound/>);

describe("NotFound component",()=>{
    it("NotFound renders",()=>{
        expect(notFound).toMatchSnapshot();
    })
})
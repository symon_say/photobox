import React ,{Component} from "react";
import PropTypes from 'prop-types';
import 'cross-fetch';
import { withRouter } from 'react-router-dom';
import {products} from '../../redux-actions/products';
import Card from '../card/';
import Loader from '../loader/';
import './products.scss';

class Products extends Component {
    constructor(){
        super();
        this.state={
            products:[],
            loading:false
        };
        this.loadProducts =this.loadProducts.bind(this);
    }

    componentDidMount(){
        this.setState({loading:true});
        this.loadProducts();
    }

    loadProducts(){
        return fetch("http://localhost:3000/api/products").then(response => response.json()).then(data => {
            this.setState({products:[...data],loading:false});
        }).catch(error=>{
            this.setState({loading:false}); 
        });  
    }

    render(){
        const {loading,products} = this.state;
        return(
            <div>
                {(loading) ? <Loader message='Loading products...'/> :null}
                <div id="products" >
                    {products.map((product,i )=>{
                        return <Card index={i}  key={i} {...product} />
                    })}
                </div>
            </div>
        )
    }
}

export default Products;


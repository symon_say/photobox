import React from 'react';
import Products from './index';
import Card from '../card/';
import { shallow,mount } from 'enzyme';
import fetchMock from 'fetch-mock';

require ('../../setupTests.js');

describe("Products component",()=>{
    const mockProducts = [{
        "title": "Simple Canvas",
        "description": "Lets your pictures speak for themselves.",
        "image": {
            "path": "/images/product.jpg",
            "alt": "Simple Canvas"
        },
        "price": 1500,
        "currency": "£",
        "priceLabel": "From",
        "productLabel": "bestseller",
        "cta": "Shop Now",
        "ctaLink": "/random/link/to/no/where"
    }, {
        "title": "Collage Canvas",
        "description": "Can't choose just one pic? Put your favourite photos on one canvas - a collage.",
        "image": {
            "path": "/images/product.jpg",
            "alt": "Collage Canvas"
        },
        "price": 2500,
        "currency": "£",
        "priceLabel": "From",
        "productLabel": "",
        "cta": "Shop Now",
        "ctaLink": "/random/link/to/no/where"
    }]

    fetchMock.get("http://localhost:3000/api/products",mockProducts);

    let getProductsMock= jest.fn();
   
    it("Products renders",()=>{
        expect(Products).toMatchSnapshot();
    })
    it('Initialize state ',()=>{
        let products = shallow(<Products/>);    
        expect(products.state()).toEqual({ products: [], loading: true })
    });
    it("Show loader when fetching products ",()=>{
        const products = mount(<Products/>);
        const loader = products.find("Loader");
       
        expect(loader.exists()).toBe(true);
        expect(loader.text()).toEqual("Loading products...");
    });
    it("Call fetch products on componentDidMount",()=>{
        const spy = jest.spyOn(Products.prototype, 'loadProducts');
        const wrapper = mount(<Products />);  
        expect(spy).toHaveBeenCalledTimes(1);
    })
    it("Fetch the products and set the state of Products and render the 2 Cards", async()=>{
        let products = mount(<Products/>);
         await products.instance().loadProducts().then(()=>{
            products.update();
            expect(products.state().products).toEqual(mockProducts);
            expect(products.find("Card").length).toEqual(2)
        }).catch(err =>{
            console.log(err)
        })
    });
})
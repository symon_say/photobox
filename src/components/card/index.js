import React from 'react';
import PropTypes from 'prop-types';
import './card.scss';
import Price from '../price';

const url ='http://localhost:3000';

class Card extends React.PureComponent {

  render(){
    const {image,productLabel,title,description,priceLabel,price,currency,cta,ctaLink} = this.props
    return (
      <div className="grid-item" >
        <div className="card" >
          <div className="card-img" >
            { image ?  <img src={`/dist${image.path}`} alt={image.alt}  /> : null }
          </div>
          {productLabel!=="" ? <div className="best-seller">{productLabel}</div> : <div className='best-seller-gap' /> } 
            <h2>{title}</h2>
            <p>{description}</p>
            <div className="price-text">{priceLabel} <Price price={price} currency={currency}/></div>
            <a  href={ctaLink}>{cta}</a>
        </div>
      </div>
    )
  }
}

export default Card;

Card.propTypes = {
  title:PropTypes.string,
  description:PropTypes.string,
  productLabel:PropTypes.string,
  priceLabel:PropTypes.string,
  price:PropTypes.number,
  currency:PropTypes.string,
  price:PropTypes.number,
  cta:PropTypes.string,
  ctaLink:PropTypes.string,
  image: PropTypes.shape({
    path: PropTypes.string,
    alt: PropTypes.string
  }),
}
import React from 'react';
import Card from './index';
import { shallow } from 'enzyme';

require ('../../setupTests.js');

const props ={
  title:"product title",
  description:"product description",
  productLabel:"best seller",
  priceLabel:"price label",
  price:2500,
  currency:"£",
  cta:"shop now",
  ctaLink:"http://buy/now",
  image: {
    path: "/path/to/file",
    alt: "some text"
  },
}

describe("Card component",()=>{

    it("Card renders",()=>{
        const card = shallow(<Card {...props}/>);
        expect(card).toMatchSnapshot();
    })
    it("Have a class of `card`",()=>{
        const card = shallow(<Card {...props}/>);
        expect(card.find(".card").exists()).toBe(true);
    })

    const card = shallow(<Card {...props}/>);

    it("It renders title text",()=>{
        expect(card.find("h2").text()).toEqual("product title");
    });
    it("It renders description text",()=>{
        expect(card.find("p").text()).toEqual("product description");
    });
    it("It renders the Proce component",()=>{
        expect(card.find("Price").exists()).toBe(true);
    });
    it("It renders the price text",()=>{   
        expect(card.find(".price-text").text()).toEqual("price label <Price />");
    });
    it("It renders the cta text",()=>{    
        expect(card.find("a").text()).toEqual("shop now");
    });
    it("It renders the cta link",()=>{    
        expect(card.find('a').props().href).toBe('http://buy/now');
    });

    it("Call the price with the right props",()=>{    
        expect(card.find("Price").props()).toEqual({ price: 2500, currency: '£' });
    });
    it("Show best seller only",()=>{
        expect(card.find(".best-seller").text()).toEqual("best seller")
    });
    it("Show best seller only if it is available",()=>{
        props.productLabel = ""; 
        const card = shallow(<Card {...props}/>);
        expect(card.find(".best-seller").exists()).toBe(false);
    });
    it("Show best seller gap if not available",()=>{
        props.productLabel = ""; 
        const card = shallow(<Card {...props}/>);
        expect(card.find(".best-seller-gap").exists()).toBe(true);
    });
    it("It renders the image src",()=>{
        expect(card.find("img").props().src).toEqual("/dist/path/to/file");
    });
    it("It renders the image alt",()=>{
        expect(card.find("img").props().alt).toEqual("some text");
    });
    it("Show the image  only if it is available",()=>{
        delete props.image ; 
        const card = shallow(<Card {...props}/>);
        expect(card.find("img").exists()).toBe(false);
    });
})
import React from 'react';
import Loader from './index'
import { shallow } from 'enzyme';

require ('../../setupTests.js');

const props ={
    message:"Loading stories...",
}

const loader = shallow(<Loader {...props}/>);

describe("Loader component",()=>{

    it("Loader renders",()=>{
        expect(loader).toMatchSnapshot();
    })
    it("Have a class of `loader`",()=>{loader.debug();
        expect(loader.find(".loader").exists()).toBe(true);
    })
    it("It render the text correctly",()=>{loader.debug();
        expect(loader.find(".loader").text()).toEqual("Loading stories...");
    })
})
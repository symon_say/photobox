import React from 'react';
import PropTypes from 'prop-types';
import './loader.scss';

const Loader = (props) => {
    return <div className='loader'>{props.message}</div>
 }

export default Loader;

Loader.PropTypes = {
    message:PropTypes.string,
}
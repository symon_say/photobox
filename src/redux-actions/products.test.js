
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import {FETCH_PRODUCTS_START,FETCH_PRODUCTS_ERROR,FETCH_PRODUCTS_END} from './constants';
import {products} from './products';

const createMockStore = configureMockStore([thunk]);
const store = createMockStore({bitcoin:{}});
const mockResponse = [{
    "title": "Simple Canvas",
    "description": "Lets your pictures speak for themselves.",
    "image": {
        "path": "/images/product.jpg",
        "alt": "Simple Canvas"
    },
    "price": 1500,
    "currency": "£",
    "priceLabel": "From",
    "productLabel": "bestseller",
    "cta": "Shop Now",
    "ctaLink": "/random/link/to/no/where"
}]; 

fetchMock.get('https://localhost/api/products', mockResponse);

it('create an async action to fetch the Products response',() => {
    
    const expectedActions = [ { type: 'FETCH_PRODUCTS_START',
    payload: { loading: true, products: [] } },
  { type: 'FETCH_PRODUCTS_END',
    payload: { loading: true, products: [] } }];
    
    store.dispatch(products()).then(()=>{
        expect(store.getActions()).toEqual(expectedActions);
    })
})
import fetch from 'cross-fetch';
import * as Constants from './constants';
const URL = 'http:localhost:3000';

export function products() {
    return dispatch => {
       
        dispatch({type:Constants.FETCH_PRODUCTS_START, payload: {loading: true, products:[]}});
       
       return fetch(`${URL}/api/session`)
            .then(data => data.json())
            .then(producs => {
                dispatch({type:Constants.FETCH_PRODUCTS_END, payload: {loading: true, products:[...data]}});
            })
            .catch(err=>{
                dispatch({type:Constants.FETCH_PRODUCTS_END, payload: {loading: true, products:[]}});
            });
    };
}



import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import routers from './index';
import HomePage from '../views/HomePage';
import NotFound from '../components/notFound';
import { shallow,mount } from 'enzyme';
import fetchMock from 'fetch-mock';
require ('../setupTests.js');

const mockProducts = [{
    "title": "Simple Canvas",
    "description": "Lets your pictures speak for themselves.",
    "image": {
        "path": "/images/product.jpg",
        "alt": "Simple Canvas"
    },
    "price": 1500,
    "currency": "£",
    "priceLabel": "From",
    "productLabel": "bestseller",
    "cta": "Shop Now",
    "ctaLink": "/random/link/to/no/where"
}, {
    "title": "Collage Canvas",
    "description": "Can't choose just one pic? Put your favourite photos on one canvas - a collage.",
    "image": {
        "path": "/images/product.jpg",
        "alt": "Collage Canvas"
    },
    "price": 2500,
    "currency": "£",
    "priceLabel": "From",
    "productLabel": "",
    "cta": "Shop Now",
    "ctaLink": "/random/link/to/no/where"
}]

fetchMock.get("http://localhost:3000/api/products",mockProducts);

describe("Test Routes",() => {
    it("Test homepage", ()=>{
        const sut = mount(<MemoryRouter initialEntries={[ '/' ]} initialIndex={0}><HomePage/></MemoryRouter>);
        expect(sut.find("HomePage").exists()).toBe(true);  
    })
    it("Test 404 page", ()=>{
        const sut = mount(<MemoryRouter initialEntries={[ '/suus' ]} initialIndex={0}><NotFound/></MemoryRouter>);
        expect(sut.find("NotFound").exists()).toBe(true);  
    })
});

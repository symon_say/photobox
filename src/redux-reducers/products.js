
import * as Constants from '../redux-actions/constants';

const initialState = {
    products: [],
    loading:false
};

export default function products(state = initialState, action){
    switch (action.type) {
        case Constants.FETCH_PRODUCTS_START:
            return {loading:true, products:[]};
        case Constants.FETCH_PRODUCTS_END:
            return {loading:false, products:[...action.payload.products]};
        case Constants.FETCH_PRODUCTS_ERROR:
            return {loading:false, products:[]};
        default:
            return state;
    }
}

import React from 'react';
import '../stylesheets/listingPage.scss';
import Products from '../components/products'
const HomePage = () => {
    return (
        <main>
            <Products/>
        </main>
    );
};

export default HomePage;